package com.ding.bustqq.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
* 服务端
* */
public class MyServer {

    public static void main(String[] args) {
        ServerSocket server = null;
        Socket socket = null;

        try {
            server = new ServerSocket(8888);
            ExecutorService pool = Executors.newFixedThreadPool(10);

            while (true) {
                socket = server.accept();
                pool.execute(new MyThead(socket));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


class MyThead extends Thread {

    private Socket socket;
    BufferedReader reader = null;
    PrintWriter writer = null;

    public MyThead(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {

        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(socket.getOutputStream(), true);
            Scanner scanner = new Scanner(System.in);
            while (true) {
                String line = reader.readLine();
                System.out.println(line + "!!!!");
 
                String message = scanner.nextLine();
                writer.println(message);
                writer.flush();
                
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
                writer.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}