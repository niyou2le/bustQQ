package com.ding.bustqq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BustqqApplication {

    public static void main(String[] args) {
        SpringApplication.run(BustqqApplication.class, args);
    }

}

